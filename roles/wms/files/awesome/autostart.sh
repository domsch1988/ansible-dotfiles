#!/bin/sh

run() {
	if ! pgrep -f "$1"; then
		"$@" &
	fi
}

run "/home/dominik/.screenlayout/layout.sh"
run "picom"
